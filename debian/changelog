cpluff (0.2.0+ds1-2) unstable; urgency=medium

  * Remove myself from Uploaders:

 -- Balint Reczey <balint@balintreczey.hu>  Sun, 23 Jan 2022 22:30:19 +0100

cpluff (0.2.0+ds1-1) unstable; urgency=medium

  * Move packaging repository to Salsa
  * Add basic Salsa CI configuration
  * debian/copyright: Exclude C++ API documentation, too, from the upstream
    tarball
  * debian/watch: Add +ds1 repack suffix
  * New upstream version 0.2.0
  * Update symbols
  * Use debhelper-compat instead of debian/compat and bump level to 12
  * debian/libcpluff0.post*: Drop empty maintainer scripts
  * debian/control, debian/watch: Use HTTPS to access www.c-pluff.org
  * debian/control:
    - Build-depend on pkg-config and doxygen
    - Use my rbalint@ubuntu.com email address as the Uploader
    - Bump standards version

 -- Balint Reczey <rbalint@ubuntu.com>  Sun, 03 May 2020 14:13:55 +0200

cpluff (0.1.4+dfsg1-1) unstable; urgency=medium

  * Add watch file
  * Add upstream homepage
  * Tidy up debian/control using cme
  * Imported Upstream version 0.1.4+dfsg1
    - Merged cp_load_plugin_descriptor_from_memory() by Jonathan Marshall from
      Kodi/XBMC codebase and added a unit test for it.
    - Merged German translation by Chris Leick from Kodi/XBMC codebase.
      (Closes: #702817)
    - Updated build environment for current Autotools versions.
    - More complete distribution auto-checking.
    - Minor fixes and improvements.
  * Drop obsolete patch integrated upstream
  * Update symbols file
  * Build only PIC binaries
  * Fix short description of cpluff-loader
  * Remove prebuilt API documentation from source package

 -- Balint Reczey <balint@balintreczey.hu>  Sun, 20 Mar 2016 19:25:09 +0100

cpluff (0.1.3-3) unstable; urgency=medium

  * Add myself as uploader
  * Bump libreadline dependency
  * Migrate to modern Debhelper
  * Fix tests
  * Use multiarch
  * Build-depend on dh-autoreconf
  * Bump standards version
  * Modernize variable substitution in debian/control
  * Use symbols instead of shlibs file to track library changes
  * Convert copyright file to machine-readable format
  * List packaging repository in debian/control
  * Upload to Debian (Closes: #792900)
  * Add libtool-bin to build dependencies to fix tests

 -- Balint Reczey <balint@balintreczey.hu>  Wed, 29 Apr 2015 08:27:24 +0200

cpluff (0.1.3-2) unstable; urgency=low

  * Previous version was accidentally compiled in and for testing release
    of Debian (Etch). Recompiled for stable release (Sarge).

 -- Johannes Lehtinen <johannes.lehtinen@iki.fi>  Sun,  8 Apr 2007 06:49:21 +0300

cpluff (0.1.3-1) unstable; urgency=low

  * C-Pluff release 0.1.3.
  * Improved Windows support.

 -- Johannes Lehtinen <johannes.lehtinen@iki.fi>  Fri,  6 Apr 2007 16:56:38 +0300

cpluff (0.1.2-1) unstable; urgency=low

  * C-Pluff release 0.1.2.
  * Include locale data.
  * Include example source code and cpluff-console binary in package
    libcpluff0-dev.
  * Prevent compression of HTML documentation and example source code.
  * Added a notice about C-Pluff Console binary being covered by the
    GNU GPL.

 -- Johannes Lehtinen <johannes.lehtinen@iki.fi>  Wed, 28 Mar 2007 12:30:57 +0300

cpluff (0.1.1-1) unstable; urgency=low

  * Initial Debian package.

 -- Johannes Lehtinen <johannes.lehtinen@iki.fi>  Tue, 20 Mar 2007 12:55:27 +0200
